(function() {
    'use strict';

    angular.module('reedsy.controllers', ['reedsy.services']);
})();