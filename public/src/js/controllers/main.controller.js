(function () {
    'use strict';

    MainCntrl.$inject = ['SocketService', 'ConversionService', 'Flash'];
    angular
        .module('reedsy.controllers')
        .controller('MainCntrl', MainCntrl);

    /* @ngInject */
    function MainCntrl(SocketService, ConversionService, Flash) {
        var vm = this;
        vm.conversions = [];
        vm.notifications = [];
        vm.addConversion = addConversion;

        activate();

        function activate() {

            SocketService.on('job~finished', function (job) {
                console.log("socket notification job finished: ", job);

                var message = '<strong>Conversion finished!</strong> ' + job.name +  ' successfully converted.';
                var id = Flash.create('success', message, 0, {class: 'custom-class', id: 'custom-id'}, true);

                updateConversion(job);
                vm.notifications.push(job);
            });

            SocketService.on('job~processing', function (job) {
                console.log("socket notification job processing: ", job);
                                                
                var message = '<strong>Conversion started!</strong> Processing of ' + job.name +  ' has started.';
                var id = Flash.create('info', message, 0, {class: 'custom-class', id: 'custom-id'}, true);
                updateConversion(job);
                vm.notifications.push(job);
            });

            return getConversions().then(function () {
                console.log('Activated Main Controller');
            });
        }

        function getConversions() {
            return ConversionService.getConversions().then(function (result) {
                console.log("controller conversions", result.data);
                vm.conversions = result.data;
                console.log("bind conversions", vm.conversions);
                return vm.conversions;
            });
        }

        function updateConversion(conversion) {
            for(var c = 0; c < vm.conversions.length; c++)
            {
                if(vm.conversions[c]._id === conversion._id) {
                    vm.conversions[c] = conversion;
                }
            }
        }

        function addConversion(type) {
            ConversionService.addConversion(type)
                .then(function (result) {
                    console.log("new conversion", result);
                    vm.conversions.push(result.data);
                });
        }
    }
})();
