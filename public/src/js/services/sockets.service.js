(function() {
    'use strict';
    
    SocketService.$inject = ['socketFactory'];
    angular
        .module('reedsy.services')
        .factory('SocketService', SocketService);

    /* @ngInject */
    function SocketService(socketFactory) {
        return socketFactory();
    }
})();
