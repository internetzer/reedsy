(function() {
    'use strict';

    ConversionService.$inject = ['$http', '$location', '$q'];
    angular
        .module('reedsy.services')
        .factory('ConversionService', ConversionService);

    /* @ngInject */
    function ConversionService($http, $location, $q) {
        var isPrimed = false;
        var primePromise;

        var service = {
            getConversions: getConversions,
            addConversion: addConversion
        };

        return service;

        function getConversions() {
            var deferred = $q.defer();

            $http.get('/api/conversions')
                .then(function success(data) {
                    console.log("conversions", data);
                    deferred.resolve(data);
                }, function error(err) {
                    console.log(err);
                    deferred.reject(err);
                });

            return deferred.promise;
        }

        function addConversion(type) {
            var deferred = $q.defer();

            $http.post('/api/conversion', { type: type })
                .then(function success(data) {
                    console.log("new conversion", data);
                    deferred.resolve(data);
                }, function error(err) {
                    console.log(err);
                    deferred.reject(err);
                });
            return deferred.promise;    
        }
    }
})();
