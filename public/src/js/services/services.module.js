(function() {
    'use strict';

    angular.module('reedsy.services', ['btford.socket-io']);
})();