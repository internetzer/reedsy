'use strict';

module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    jshint: {
      all: ['public/src/js/**/*.js'] 
    },

    wiredep: {
      target: {
        src: [
          'public/views/index.html'
          ]
      }
    },

    ngAnnotate: {
      options: {
        singleQuotes: true
      },
      all: {
        files: [
          {
            expand: true,
            src: ['public/src/js/**/*.js'],
            dest: '.',
          },
        ],
      }
    },

    uglify: {
      build: {
        files: {
          'public/dist/js/app.min.js': ['public/src/js/**/*.module.js', 'public/src/js/**/*.js']
        }
      }
    },

    less: {
      build: {
        files: {
          'public/dist/css/main.css': 'public/src/css/main.less'
        }
      }
    },

    cssmin: {
      build: {
        files: {
          'public/dist/css/main.min.css': 'public/dist/css/main.css'
        }
      }
    },

    watch: {
      css: {
        files: ['public/src/css/**/*.less'],
        tasks: ['less', 'cssmin']
      },
      js: {
        files: ['public/src/js/**/*.js'],
        tasks: ['jshint', 'ngAnnotate', 'uglify']
      },
      options: {
        livereload: true
      }
    },

    nodemon: {
      dev: {
        script: 'server/app.js'
      }
    },

    concurrent: {
      options: {
        logConcurrentOutput: true
      },
      tasks: ['nodemon', 'watch']
    }   

  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-wiredep');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-nodemon');
  grunt.loadNpmTasks('grunt-concurrent');

  grunt.registerTask('default', ['wiredep', 'less', 'cssmin', 'jshint', 'ngAnnotate', 'uglify', 'concurrent'])
  };