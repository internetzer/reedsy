'use strict';

var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var router = require(__dirname + '/api');
var config = require(__dirname + '/config/database');
var queuingService = require(__dirname + "/api/services/queuingService");

mongoose.connect(config.database, {useMongoClient: true});
mongoose.connection.on('connected', function() {
    console.log('connected to database ' + config.database);
});

var app = express();  

app.use(express.static('./public'));
app.use('/bower_components', express.static('./bower_components'));
app.use('/dist', express.static('./public/dist'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', router);

app.get('/', function(req, res) {
    res.sendfile('./public/views/index.html');
});

var server = require('http').createServer(app);  
var io = require('socket.io')(server);

io.on('connection', function(socket) {
    console.log("incoming socket connection...");
    queuingService.setSocket(socket);
});

server.listen(3000, function startup() {
    console.log("app listening...")
    queuingService.loadQueue();
});
console.log('server is running on port 3000');