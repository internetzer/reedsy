'use strict';

var queue = require('queue');
var Conversion = require('../../models/conversion');
var queuingService = {};
var q = queue();
var _socket;
q.autostart = true;
q.concurrency = 1;

queuingService.setSocket = function setSocket(socket) {
    _socket = socket;
}

queuingService.loadQueue = function loadQueue() {

    Conversion.find({ status: ['Queued', 'Processing'] }).exec(function(err, conversions) {
        if(err) {
            console.log("error loading conversions for queue");
        }
        else {
            console.log('found conversions: ', conversions);
            //queue conversions and start
            for(let i = 0; i < conversions.length; i++)
            {
                q.push(function(cb) {
                    setTimeout(function() {
                        //mark conversion as processed
                        Conversion.update({ _id: conversions[i]._id }, { status: 'Processed' }, function (err, updated) {
                            if (err) {
                                console.log('Error while updating conversion', err);
                            }
                            else {
                                console.log('Conversion updated', updated);
                            }
                            cb();
                        });
                    }, 10000);
                });
            }
        }
    });

    console.log("queue loaded!");
}

queuingService.addConversion = function addConversion(type) {

    return new Promise(function(resolve, reject) {
        Conversion.find({ type: type }).exec(function(err, conversions) {
            if (err) {
                reject(err);
            } else {
                if (conversions) {
                    var con = new Conversion();
                    con.name = type + " #" + (conversions.length + 1);
                    con.type = type;
                    con.status = "Queued";
                    var timeout = 10000;
                    if(type === "PDF") {
                        timeout = 100000;
                    }

                    // save the conversion
                    con.save(function(err) {
                        if (err) {
                            reject(err);
                        } else {
                            //add saved conversion to queue
                            q.push(function(cb) {
                                //mark conversion as processed
                                Conversion.findOneAndUpdate({ _id: con._id }, { status: 'Processing' }, {new: true}, function (err, updated) {
                                    if (err) {
                                        console.log('Error while updating conversion', err);
                                    }
                                    else {
                                        console.log('Conversion updated', updated);
                                        _socket.emit('job~processing', updated);
                                    }
                                });

                                setTimeout(function() {
                                    //mark conversion as processed
                                    Conversion.findOneAndUpdate({ _id: con._id }, { status: 'Processed' }, {new: true}, function (err, updated) {
                                        if (err) {
                                            console.log('Error while updating conversion', err);
                                        }
                                        else {
                                            console.log('Conversion updated', updated);
                                            _socket.emit('job~finished', updated);
                                        }
                                        cb();
                                    });
                                }, timeout);
                            });
                            return resolve(con);
                        }
                    });
                } else {
                    return resolve({});
                }
            }
        });    
    });
}

// get notified when jobs complete
q.on('success', function(result, job) {
  console.log('job finished processing:', job.toString().replace(/\n/g, ''));
});

module.exports = queuingService;