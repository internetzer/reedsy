'use strict';

var express = require('express');
var queue = require('queue');
var conversions = require('../mock/conversions.json');
var Conversion = require('../models/conversion');
var queuingService = require('../api/services/queuingService');

var router = express.Router();
var q = queue();

router.get('/conversions', function(req, res) {

    Conversion.find(function (err, data){
        if(err) {
            res.send(err);
        }
        else {
            res.json(data);
        }
    });
    //res.json({conversions: conversions});
});

router.post('/conversion', function(req, res) {
    queuingService.addConversion(req.body.type).then( function success(result) {
        res.json(result);
    }, function error(err) {
        res.send(err);
    });
});

module.exports = router;