var mongoose = require('mongoose');
var config = require('../config/database');

var ConversionSchema = mongoose.Schema({
    name: {
        type: String
    },
    type: {
        type: String
    },
    status: {
        type: String
    },
    createdAt: { type: Date, default: Date.now }
});

var Conversion = module.exports = mongoose.model('Conversion', ConversionSchema);